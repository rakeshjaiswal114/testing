import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  @Input() acceptItem: string;
  @Input() arrItem: any;
  searchDisplayMessage=""
  ngOnChanges() {
    console.log(this.acceptItem);
    console.log(this.arrItem);
    if (this.arrItem.length == 0) {
      this.searchDisplayMessage = "No Records Found"
    }
    else{
      this.searchDisplayMessage = ""
    }
   }
  constructor() { }
  ngOnInit() {

  }

  

}
